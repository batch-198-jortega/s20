console.log("Hello World!")

// Solution 1:

let number = prompt("Give me a number: ");

number = parseInt(number, 10);

for(let x = number; x >= 0; x--) {

	if(x <= 50){

		console.log("The current value is at 50. Terminating the loop");
		break;

	}

	if(x % 10 === 0){

		console.log("The number is divisible by 10. Skipping the number.");
		continue;

	}

	if(x % 5 === 0){

		console.log(x);

	}

}

// Multiplcation Table of 5
// Solution 2:

function showTable(num){
	for(let count = 1; count <= 10; count++) {
		let product = num * count;
		console.log(num + " x " + count + " = " + product);
	}
}

showTable(5);